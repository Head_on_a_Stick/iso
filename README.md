# Archlabs-Iso

Source for ArchLabs ISO builds


### Building the ISO

The package `archiso` must be installed and it needs to be built on an `Arch x86_64` system.

First clone the repo to your system.

    git clone https://bitbucket.org/archlabslinux/iso ~/Downloads/iso


Next make a directory to build the ISO in. It can be anywhere you like, it's just a place to work.

    mkdir ~/build


Now you need to move the entire `archlabs` folder into the new directory that you made.  
**NOTE: It is very important that you do it as root**. Every file needs to be owned by root.  
This can be done by opening a file manager as root, or from a terminal using `su` or `sudo`.

If you cloned the repo and made a directory as shown above, then you can run

    cp -r ~/Downloads/iso/archlabs ~/build/

Before building you will need to clean your pacman cache.

    pacman -Scc


Now just run the build script with verbose *(`-v`)* for more output.

    ./build -v


When finished there will be a directory called `out`, the ISO will be in there.


### Customization

- `packages.x86_64` lists the packages that will be installed on the ISO.

- `efiboot` and `syslinux` contain boot configuration files.

- `pacman.conf` is used while building and has entries for the `archlabs_repo` and `archlabs_unstable` repos.

- `airootfs` is the live boot file system. This is where to add anything that should be included on the ISO.
Remember, **everything must be done as root**, if you add something, do it with `su` or `sudo`.

When finished, run the `build` script in the main directory.
